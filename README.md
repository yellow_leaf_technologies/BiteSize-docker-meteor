#### Installing docker for BiteSize apps
##### Installation requirements
    docker
    docker-engine

For the docker installation instruction follow this [link](https://docs.docker.com/install/).

Create new directory called BiteSize and change current directory to newly created.
##### Cloning projects
Git clone these `client`, `python-app`, `celery-app` projects to current directory.

Git clone current docker project to BiteSize directory.

##### Copy docker-compose.yml

Copy docker-compose.yml and .dockerignore files from docker directory to the root of BiteSize directory.

File structure of BiteSize should be:
```
    ...
    BiteSize
    |    client
    |    python-app
    |    docker
    |    |    client
    |    |    |    Dockerfile
    |    |    db
    |    |    |   Dockerfile
    |    |    python-app
    |    |    |    Dockerfile
    |    |    |    .env
    |    |    celery-app
    |    |    |    Dockerfile
    |    |    |    .env
    |    |   wait-for-it.sh
    |    docker-compose.yml
    |    .dockerignore
    ...  
```
#### Development fixes
##### In `client` application 
(Windows/Unix development popular bug)

Fix case problem with `client/client/main.scss` file. 

From this
 ```
 @import "../imports/components/resetpassword/styles.scss";
 ```
 to this
 ```
 @import "../imports/components/resetPassword/styles.scss";
 ```

Fix case problem with `client/components/phoenix/phoenix.js`

From this
 ```
 import { name as ResetPassword } from '../resetpassword';
 ```
to this
 ```
 import { name as ResetPassword } from '../resetPassword';
 ```
##### In `python-app`

edit packages.json file like this:
```
"scripts": {
    "redis-dev": "REDIS_URL=redis://redis:6379 REDIS_HOST=redis REDIS_PORT=6379 REDIS_PASSWORD=;",
    "postgres-dev": "POSTGRES_HOST=postgres POSTGRES_PORT=5432 POSTGRES_DB=bitesize-dev POSTGRES_USER=root POSTGRES_PASSWORD=password",
    "mongo-dev": "MONGO_URL=mongodb://database:27017/meteor",
    ...
     "dev": "IS_DEVELOPMENT=true yarn redis-dev yarn postgres-dev yarn mongo-dev yarn twilio-dev FLASK_APP=main.py python main.py",
    ...
}
```
Next run the following command 
```
docker-compose up 
```
